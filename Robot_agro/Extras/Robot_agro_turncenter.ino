#include "Arduino.h"
int stopPinDiod = 12;
#define SPEED_1 6
#define DIR_1   7

 
int buttonPin = 11; 
int buttonState = 0; 
boolean buttonReady = true;
int witePin = 8;
int blackPin = 9;
int redPin = 10;
boolean currentDirection = true;

void setup() {
  // put your setup code here, to run once:
  pinMode(stopPinDiod, OUTPUT);
     
     pinMode(witePin, INPUT);
     pinMode(blackPin, INPUT);
     pinMode(redPin, INPUT);
     
     pinMode(SPEED_1, OUTPUT);
     pinMode(DIR_1, OUTPUT); 
     pinMode(buttonPin, INPUT);
     

     while(!itCenter()) {
      delay(600);
      boolean lastDirection = currentDirection;
      stopOrNot();
          if(lastDirection != currentDirection) {
            analogWrite(SPEED_1, 0);
            delay(200);
          }
        
        goAway(150, currentDirection);
        
     }
     analogWrite(SPEED_1, 0);
     Serial.begin(9600); 
}

void loop() {
  
 if(buttonReady) {
    if(digitalRead(buttonPin)) {
      delay(10);
      if (digitalRead(buttonPin)) {
        buttonReady = false;
        long second = millis();
        boolean lastDirection = currentDirection;
        while (millis() - second < 300) {
          stopOrNot();
          if(lastDirection != currentDirection) {
            analogWrite(SPEED_1, 0);
            return; 
          }
          
          goAway(200, currentDirection);
          delay(5);
        }
        analogWrite(SPEED_1, 0);
        
      }
    }
  
 } else {
  if(digitalRead(buttonPin) == LOW) buttonReady = true;
 }
}


void goAway(int mySpeed, boolean myDirection) {
  digitalWrite(DIR_1, myDirection);
  analogWrite(SPEED_1, mySpeed);
}

 void stopOrNot() {
  
  if (digitalRead(redPin) == HIGH && digitalRead(blackPin) == HIGH && digitalRead(witePin) == HIGH) {
    //digitalWrite(stopPinDiod, HIGH);
    currentDirection = false;
  }
  if (digitalRead(witePin) && digitalRead(blackPin) == LOW && digitalRead(redPin) == LOW) {
    //digitalWrite(stopPinDiod, HIGH);
    currentDirection = true;
  }
}

boolean itCenter() {
  
  if(digitalRead(blackPin) == HIGH && digitalRead(redPin) == HIGH && digitalRead(witePin) == LOW) {
    delay(15);
    if(digitalRead(blackPin) == HIGH && digitalRead(redPin) == HIGH && digitalRead(witePin) == LOW) {
    digitalWrite(stopPinDiod, HIGH);
    return true; 
    }
  } else {
    digitalWrite(stopPinDiod, LOW);
    return false;
  }
 
}

