#include <C:\Users\ofice\Documents\Arduino\line\DriverVrumVrum.h>
#include <Arduino.h>

DriverVrumVrum::DriverVrumVrum(int m_leftMotorSpeedPin, int m_rightMotorSpeedPin, int m_standby, int m_leftMotorFirstHalfBridge, int m_leftMotorSecondHalfBridge, int m_rightMotorFirstHalfBridge, int m_rightMotorSecondHalfBridge)
{
	 leftMotorSpeedPin = m_leftMotorSpeedPin;
	 rightMotorSpeedPin = m_rightMotorSpeedPin;
	 standby = m_standby;
	 leftMotorFirstHalfBridge = m_leftMotorFirstHalfBridge;
	 leftMotorSecondHalfBridge = m_leftMotorSecondHalfBridge;
	 rightMotorFirstHalfBridge = m_rightMotorFirstHalfBridge;
	 rightMotorSecondHalfBridge = m_rightMotorSecondHalfBridge;
	 digitalWrite(standby, HIGH);
}

void DriverVrumVrum::move(int leftMotorSpeed, int rightMotorSpeed, boolean leftReversed = false, boolean rightReversed = false){
	if (leftMotorSpeed > 0) {
		digitalWrite(leftMotorFirstHalfBridge, HIGH);
		digitalWrite(leftMotorSecondHalfBridge, LOW);
	}
	else {
		digitalWrite(leftMotorFirstHalfBridge, LOW);
		digitalWrite(leftMotorSecondHalfBridge, HIGH);
	}
	if (rightMotorSpeed < 0) {
		digitalWrite(rightMotorFirstHalfBridge, HIGH);
		digitalWrite(rightMotorSecondHalfBridge, LOW);
	}
	else {
		digitalWrite(rightMotorFirstHalfBridge, LOW);
		digitalWrite(rightMotorSecondHalfBridge, HIGH);
	}
	analogWrite(leftMotorSpeedPin, leftMotorSpeed);
	analogWrite(rightMotorSpeedPin, rightMotorSpeed);
	Serial.println(leftMotorSpeed);
};