#pragma once
#include <Arduino.h>
class DriverVrumVrum {
public:
	DriverVrumVrum(int, int, int, int, int, int, int);
	void move(int leftMotorSpeed, int rightMotorSpeed, boolean leftReversed = false, boolean rightReversed = false);
private:
	int leftMotorSpeedPin;
	int rightMotorSpeedPin;
	int standby;
	int leftMotorFirstHalfBridge;
	int leftMotorSecondHalfBridge;
	int rightMotorFirstHalfBridge;
	int rightMotorSecondHalfBridge;
};