#define DEBUG TRUE //debug flag
//#define ANALOG 
#define DIGITAL
#define MOTOR_SHIELD
//#define TB6612FNG

#include <QTRSensors/QTRSensors.h>

//------------------------------------------------------------motor configuration------------------------------------------------------------
#ifdef MOTOR_SHIELD								
const int leftMotorDirectionPin = 4; //pins of motor shield 
const int rightMotorDirectionPin = 7;
const int leftMotorSpeedPin = 5;
const int rightMotorSpeedPin = 6;
const boolean leftMotorDirection = HIGH; //direction of motors
const boolean rightMotorDirection = HIGH;
#endif
#ifdef TB6612FNG	
#include <C:\Users\ofice\Documents\Arduino\line\DriverVrumVrum.h>
DriverVrumVrum motors(5,3,2,7,8,6,4);
#endif
//------------------------------------------------------------sensor configuration-----------------------------------------------------------
#ifdef DIGITAL
const int NUM_SENSORS = 8; // number of sensors used
const int TIMEOUT = 2500;// waits for 2500 microrights for sensor outputs to go low
const int EMITTER_PIN = 12; // emitter is controlled by digital pin 12
unsigned char sensorPins[] = { 2, A0, A1, 8, A3, A4, A5, 3 }; //sensors are connected to those pins
QTRSensorsRC qtr(sensorPins, NUM_SENSORS, TIMEOUT, EMITTER_PIN);
unsigned int sensorValues[NUM_SENSORS];
#endif 
#ifdef ANALOG
const int NUM_SENSORS = 8; // number of sensors used
const int EMITTER_PIN = 12; // emitter is controlled by digital pin 12
const int NUM_SAMPLES_PER_SENSOR = 4;//average 4 analog samples per sensor reading
unsigned char sensorPins[] = {0, 1, 2, 3, 4, 5, 6, 7}; //sensors are connected to those pins
QTRSensorsAnalog qtr(sensorPins, NUM_SENSORS, NUM_SAMPLES_PER_SENSOR, EMITTER_PIN);
unsigned int sensorValues[NUM_SENSORS];
#endif 

const int leftMotorSpeed = 40;
const int rightMotorSpeed = 40;
const float kP = 0.2;
const float kI = 0.0;
const float kD = 0.0;
float error = 0.0;
float correction = 0.0;
boolean turn = false;// motors turn, false for right, true for left


void setup()
{
	delay(500);
	for (int i = 4; i < 8; i++) pinMode(i, OUTPUT);
	digitalWrite(leftMotorDirectionPin, HIGH);
	digitalWrite(13, HIGH);    // turn on Arduino's LED to indicate we are in calibration mode
	for (int i = 0; i < 100; i++)  // make the calibration take about 2 seconds
	{
		qtr.calibrate();       // reads all sensors 10 times at 2500 us per read (i.e. ~25 ms per call)
	}
	digitalWrite(13, LOW);     // turn off Arduino's LED to indicate we are through with calibration
	// print the calibration minimum values measured when emitters were on
	Serial.begin(38400);
	for (int i = 0; i < NUM_SENSORS; i++)
	{
		Serial.print(qtr.calibratedMinimumOn[i]);
		Serial.print(' ');
	}
	Serial.println();
	// print the calibration maximum values measured when emitters were on
	for (int i = 0; i < NUM_SENSORS; i++)
	{
		Serial.print(qtr.calibratedMaximumOn[i]);
		Serial.print(' ');
	}
	Serial.println();
	Serial.println();
	delay(1000);
}

void loop()
{
	error = (qtr.readLine(sensorValues) - 3500.0) / 1000.0;
	correction = error * kP;
	analogWrite(leftMotorSpeedPin, leftMotorSpeed * (1 + correction));
	analogWrite(rightMotorSpeedPin, rightMotorSpeed * (1 - correction));
}
