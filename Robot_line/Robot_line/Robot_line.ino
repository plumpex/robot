//ROBOT V1.0




#include "IRremote.h"

// Моторы подключаются к клеммам M1+,M1-,M2+,M2-
// Motor shield использует четыре контакта 6,5,7,4 для управления моторами
#define MOTOR_LEFT       6 //левый мотор
#define MOTOR_RIGHT      5 //правый мотор
#define DIR_LEFT         7 //направление левого мотора
#define DIR_RIGHT        4 //направление правого мотора
#define LEFT_SENSOR_PIN  8 //левый сенсор
#define RIGHT_SENSOR_PIN 9 //правый сенсор

// Скорость, с которой мы движемся вперёд (0-255)
#define SPEED_RIGHT            100 //скорость правого колеса
#define SPEED_LEFT             100 //скорость левого колеса

#define STEERSPEED       100 //скорость поворота

//состояния робота
#define STATE_FORWARD    0 //вперед
#define STATE_RIGHT      1 //вправо
#define STATE_LEFT       2 //налево

int state = STATE_FORWARD;

boolean raceStarted = false;

IRrecv irrecv(11); // Указываем пин, к которому подключен приемник

decode_results results;

void runForward() //функция езды вперед
{


  analogWrite(MOTOR_LEFT, SPEED_LEFT); //скорость двигателей
  analogWrite(MOTOR_RIGHT, SPEED_RIGHT);

  digitalWrite(DIR_LEFT, HIGH); //ЭКСПЕРЕМЕНТАЛЬНЫМ ПУТЕМ НАЙТИ ЗНАЧЕНИЕ HIGH ИЛИ LOW
  digitalWrite(DIR_RIGHT, HIGH); //ЭКСПЕРЕМЕНТАЛЬНЫМ ПУТЕМ НАЙТИ ЗНАЧЕНИЕ HIGH ИЛИ LOW
}

void steerRight() //поворот направо
{
  state = STATE_RIGHT;
  

  // Замедляем правое колесо относительно левого,
  // чтобы начать поворот
  analogWrite(MOTOR_RIGHT, 0);  
  analogWrite(MOTOR_LEFT, STEERSPEED);

  digitalWrite(DIR_LEFT, HIGH); //ЭКСПЕРЕМЕНТАЛЬНЫМ ПУТЕМ НАЙТИ ЗНАЧЕНИЕ HIGH ИЛИ LOW
  digitalWrite(DIR_RIGHT, HIGH); //ЭКСПЕРЕМЕНТАЛЬНЫМ ПУТЕМ НАЙТИ ЗНАЧЕНИЕ HIGH ИЛИ LOW
}

void steerLeft() //поворот налево
{
  state = STATE_LEFT;
  

  // Замедляем левое колесо относительно правого,
  // чтобы начать поворот
  analogWrite(MOTOR_RIGHT, STEERSPEED);
  analogWrite(MOTOR_LEFT, 0);

  digitalWrite(DIR_LEFT, HIGH); //ЭКСПЕРЕМЕНТАЛЬНЫМ ПУТЕМ НАЙТИ ЗНАЧЕНИЕ HIGH ИЛИ LOW
  digitalWrite(DIR_RIGHT, HIGH); //ЭКСПЕРЕМЕНТАЛЬНЫМ ПУТЕМ НАЙТИ ЗНАЧЕНИЕ HIGH ИЛИ LOW
}

void setup()
{
  // Настраивает выводы платы 4,5,6,7 на вывод сигналов
  for (int i = 4; i <= 7; i++)
    pinMode(i, OUTPUT);
  Serial.begin(9600);// инициализируем последовательный порт

    irrecv.enableIRIn(); // Запускаем прием
}

void loop()
{
  // Наш робот ездит по белому полю с чёрным треком. В обратном случае не нужно
  // инвертировать значения с датчиков
  boolean left = !digitalRead(LEFT_SENSOR_PIN);
  boolean right = !digitalRead(RIGHT_SENSOR_PIN);
  while (raceStarted == false){
  if (irrecv.decode(&results)) // Если данные пришли 
  {
    if (results.value == 0xA90){
      raceStarted = true;
      runForward();
    }
  }
  }

  // В какое состояние нужно перейти?
  int targetState;

   if (left == right) {
    // под сенсорами всё белое или всё чёрное
    // едем вперёд
     targetState = STATE_FORWARD;
   } else if (left) {
    // левый сенсор упёрся в трек
    // поворачиваем налево
     targetState = STATE_LEFT;
   } else {
    targetState = STATE_RIGHT;
   }

  switch (targetState) {
    case STATE_FORWARD:
      runForward();
      break;

    case STATE_RIGHT:
      steerRight();
      break;

    case STATE_LEFT:
      steerLeft();
      break;
  }

}



