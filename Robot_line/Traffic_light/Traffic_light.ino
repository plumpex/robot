#include <SPI.h>         //библиотека SPI интерфейса
#include <MFRC522.h>     //библиотека NFC модуля
#include <IRremote.h>    //библиотека IR передатчика

const int ledPin =  2;  // номер выхода светодиода

#define RST_PIN 9  //пин NFC модуля
#define SS_PIN 10  //

int buttonState = 0;  // переменная для хранения состояния кнопки

int card1 = 52;  //идентификатор 1 карты
int card2 = 148;  //идентификатор 2 карты

int robot1 = 0;  //кол-во кругов 1 робота
int robot2 = 0;  //кол-во кругов 2 робота

float interval = 3100.0;  //кол-во секунд для уточнения результатов кругов
float startDelay = 3000.0;  //стартовая задержка

boolean raceStarted = false;  //началась ли гонка?

boolean timeRecord1 = false;  //проверка на завершение трассы
boolean timeRecord2 = false;  //проверка на завершение трассы

MFRC522 mfrc522(SS_PIN, RST_PIN);  //создание объекта NFC модуля

IRsend irsend;  //создание объекта IR передатчика

void setup() { //настройка
pinMode(ledPin, OUTPUT);     // настройка светодиода
 
Serial.begin(9600); // инициализация последовательного порта
  
SPI.begin(); // инициализация SPI

mfrc522.PCD_Init(); // инициализация MFRC522
}
 
void loop(){

float robotTime = (millis() - interval) / 1000.0; //уточнение результата круга

if(raceStarted == false){ //начало гонки и подача команды старта
  light();
}

  if(raceStarted == true){ //считываание метки
  if ( ! mfrc522.PICC_IsNewCardPresent())
return;
// чтение карты
if ( ! mfrc522.PICC_ReadCardSerial())
return;
// показать результат чтения UID и тип метки
dump_byte_array(mfrc522.uid.uidByte);

delay(300);
  }


if(robot1 == 3){  //вывод результата 1 робота
  if(timeRecord1 == false){
  Serial.println("Robot 1 time :");
   Serial.println(robotTime);
   timeRecord1 = true;
}
}

if(robot2 == 3){  //вывод результата 2 робота
  if(timeRecord2 == false){
  Serial.println("Robot 2 time :");
   Serial.println(robotTime);
   timeRecord2 = true;
  }
}

if(timeRecord1 && timeRecord2 == true){  //остановка гонки если оба робота пришли к финишу
  stopRace();
}

}
// Вывод результата чтения данных в HEX-виде
void dump_byte_array(byte *buffer)
{

if (buffer[0] == card1){  //подсчет кругов
  robot1++;
}

if (buffer[0] == card2){ //подсчет кругов
  robot2++;
}

}

void light(){
  if (millis() > startDelay) {   
 for (int i = 0; i < 3; i++) {
    irsend.sendSony(0xa90, 12);
    delay(40);
  }  
      raceStarted = true;
      digitalWrite(ledPin, HIGH);
}
}

void stopRace(){  //функция остановки гонки
  
  for(int i; i < 4; i++){  //подача светового сигнала
  digitalWrite(ledPin, LOW);
  delay(200);
  digitalWrite(ledPin, HIGH);
  delay(200);
  
}
SPI.end();  //остановка SPI интерфейса 
}



