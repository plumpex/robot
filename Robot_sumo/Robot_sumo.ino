//----------------ВЫБОР СЕНСОРА------------------------
#define ULTRASONIC //расскоментировать, если установлен ультразвуковой сенсор
//#define INFRARED //расскоментировать, если установлен инфракрасный сенсор
//----------------ПОДКЛЮЧЕНИЕ БИБЛИОТЕК----------------
#ifdef INFRARED//если подключен инфракрасный сернсор,
#include <SharpIR.h>//то подключить библиотеку инфракрасного датчика
const int irPin = A0;//пин сенсора
const int model = 20150;//модель сенсора
SharpIR SharpIR(irPin, model);//создание объекта сенсора
#endif
#ifdef ULTRASONIC
#include <NewPing.h>
const int trigPin = 2;
const int echoPin = 3;
NewPing sonar(trigPin, echoPin, 200);
#endif
//----------------ОБОЗНАЧЕНИЕ ПИНОВ МОТОРОВ------------
const int firstMotorDirectionPin = 4; //пин, задающий направление первого мотора
const int secondMotorDirectionPin = 7; //пин, задающий направление второго мотора
const int firstMotorSpeedPin = 5; //пин управления скоростью первого мотора
const int secondMotorSpeedPin = 6; //пин управления скоростью второго мотора
//----------------СКОРОСТЬ МОТОРОВ---------------------
const int rightMotorSpeed = 59;//скорость вращения правого колеса
const int leftMotorSpeed = 60;//скорость вращения левого колеса
const int turnSpeed = 40;//скорость поворота
//int avgSpeed = (leftMotorSpeed + rightMotorSpeed) / 2;
boolean leftDirection = LOW;
boolean rightDirection = HIGH;
//----------------ОПРЕДЕЛЕНИЕ ДАТЧИКОВ ЛИНИИ-----------
const int firstRightSensor = 10;//первый правый датчик
const int firstLeftSensor = 9;//первый левый датчик
const int secondLeftSensor = 12;//второй левый датчик
const int secondRightSensor = 11;//второй правый датчик
//----------------ПЕРЕМЕННЫЕ---------------------------
const int enemyDistance = 60;//расстояние до противника
boolean readyToMove = false;//найден ли противник?
int distance; //расстояние до объекта
int currentDistance;
int prevDistance;
boolean enemyFound = false;
int delayTime;
int arcSpeed = 26; //26 см/с


#ifdef ULTRASONIC
int enemySearch() {//функция поиска противника при помощи ультразвукового датчика
  distance = sonar.ping_cm();//измерение расстояния
  return distance;//возвращаем расстояние
}
#endif

#ifdef INFRARED
int enemySearch() {//функция поиска противника при помощи инфракрасного датчика
  distance = SharpIR.distance();//измерение расстояния
  return distance;//возвращаем расстояние
}
#endif

void moving() { //функция движения
  Serial.println("moving");
  digitalWrite(firstMotorDirectionPin, LOW);//движемся вперед
  digitalWrite(secondMotorDirectionPin, LOW);
  analogWrite(firstMotorSpeedPin, 0);//выставляем скорость движения
  analogWrite(secondMotorSpeedPin, 0);
  delay(200);
  analogWrite(firstMotorSpeedPin, rightMotorSpeed);
  analogWrite(secondMotorSpeedPin, leftMotorSpeed);
}

void enemyFind() {//функция поиска противника
  while (!readyToMove) { //выполнять, пока противник не найден
    digitalWrite(firstMotorDirectionPin, rightDirection);//движемся вокруг
    digitalWrite(secondMotorDirectionPin, leftDirection);
    analogWrite(firstMotorSpeedPin, rightMotorSpeed);//выставляем скорость вращения колес
    analogWrite(secondMotorSpeedPin, leftMotorSpeed);
    currentDistance = enemySearch();
    if (currentDistance != 0 && currentDistance < enemyDistance) {//считывание с датчика
      delayTime = currentDistance;
      delay(delayTime);
      readyToMove = true;
      moving();
    }
  }
}


void setup() {
  for (int i = 4; i < 8; i++)//конфигурирование портов
    pinMode(i, OUTPUT);
  Serial.begin(38400);// инициализация последовательного порта
}

void loop() {
  if (digitalRead(firstLeftSensor) == HIGH || digitalRead(secondLeftSensor) == HIGH) {
    digitalWrite(firstMotorDirectionPin, HIGH);
    digitalWrite(secondMotorDirectionPin, HIGH);
    delay(300);
    digitalWrite(firstMotorDirectionPin, HIGH);
    digitalWrite(secondMotorDirectionPin, LOW);
    Serial.println("End of arena");
    readyToMove = false;
  }
  if (digitalRead(firstRightSensor) == HIGH || digitalRead(secondRightSensor) == HIGH) {
    digitalWrite(firstMotorDirectionPin, HIGH);
    digitalWrite(secondMotorDirectionPin, HIGH);
    delay(300);
    digitalWrite(firstMotorDirectionPin, LOW);
    digitalWrite(secondMotorDirectionPin, HIGH);
    Serial.println("End of arena");
    readyToMove = false;
  }
  enemyFind();//ищем противника

}
